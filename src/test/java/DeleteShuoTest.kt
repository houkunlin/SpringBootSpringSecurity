import cn.goour.utils.http.Http
import cn.goour.utils.http.HttpConfig
import cn.goour.utils.http.Params
import org.json.JSONObject
import org.junit.Test

class DeleteShuoTest {
    val cookie = "pgv_pvi=5046762496; RK=4dU7ung/a2; tvfe_boss_uuid=7da03e05cc7943e2; o_cookie=1184511588; pac_uid=1_1184511588; __Q_w_s__QZN_TodoMsgCnt=1; __Q_w_s_hat_seed=1; _ga=GA1.2.48479048.1507560040; ptui_loginuin=248725655; pgv_pvid=2588145936; zzpaneluin=; zzpanelkey=; pgv_si=s6227875840; _qpsvr_localtk=0.9674625610274301; ptisp=ctc; ptcz=f71ebb2242313bc0e516652f5b6d339a775c20d80fa25ce718ae15402d4a8226; uin=o1873669577; skey=@4HCBuYYt0; pt2gguin=o1873669577; p_uin=o1873669577; pt4_token=dLskJX2rImrXpvIRAYh0ex0N3mdf8f8gpGWlXvCCteE_; p_skey=r2r5S1VYJvvrJ01BV0HTpAFje-Kmh*YUuxkDaEbKsmA_; pgv_info=ssid=s3940512470; Loading=Yes; QZ_FE_WEBP_SUPPORT=1; cpu_performance_v8=39; verifysession=h013fd4eb421bffc93e6b9c7fe14c1d8e3e4ab11b4f9973d19e75a8f14150c7082d3e244cfa55e97fc1"
    val qzonetoken = "38b2befa57e878a11ff1ca265a600e55cfdbf4ebd7e940ee37d5b752cac4a04dc5614fa3a57a5cabf0"
    @Test
    @Throws(Exception::class)
    fun testDelete() {
        var ok = false
        while (true) {
            val re = getPage(2)
            val json = JSONObject(re)
            val msglist = json.getJSONArray("msglist")
            val len = msglist.length()
            var j = 0
            while (j < len) {
                val item = msglist.getJSONObject(j++)
                println("item=" + item)
                delete(item.getString("tid"))
                Thread.sleep(2000L)
                println()
            }
            println()
//            break
            if (len == 0) {
                ok = true
            }
            if (ok) {
                break
            }
        }
    }

    fun getPage(page: Int): String {
        val listConfig = HttpConfig("https://h5.qzone.qq.com/proxy/domain/taotao.qq.com/cgi-bin/emotion_cgi_msglist_v6")
        val listParmas = Params()
        listParmas.add("uin", "1873669577")
        listParmas.add("inCharset", "utf-8")
        listParmas.add("outCharset", "utf-8")
        listParmas.add("hostUin", "1873669577")
        listParmas.add("notice", "0")
        listParmas.add("sort", "0")
        listParmas.add("pos", page * 20)
        listParmas.add("num", "20")
        listParmas.add("cgi_host", "http://taotao.qq.com/cgi-bin/emotion_cgi_msglist_v6")
        listParmas.add("code_version", "1")
        listParmas.add("format", "json")
        listParmas.add("need_private_comment", "1")
        listParmas.add("g_tk", "231531412")
        listParmas.add("qzonetoken", qzonetoken)
        listConfig.params = listParmas
        listConfig.setHeader("cookie", cookie)
        listConfig.referer = "https://qzs.qq.com/qzone/app/mood_v6/html/index.html"
        listConfig.userAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36"
        listConfig.isAjax = true

        val re = Http.get(listConfig)
        val result = String(re)
        println("getPage=" + result)
        return result
    }

    @Throws(Exception::class)
    fun delete(tid: String) {
        val listConfig = HttpConfig("https://user.qzone.qq.com/proxy/domain/taotao.qzone.qq.com/cgi-bin/emotion_cgi_delete_v6?qzonetoken=$qzonetoken&g_tk=231531412")
        val listParmas = Params()
        listParmas.add("hostuin", "1873669577")
        listParmas.add("tid", tid)
        listParmas.add("t1_source", "1")
        listParmas.add("code_version", "1")
        listParmas.add("format", "fs")
        listParmas.add("qzreferrer", "https://user.qzone.qq.com/1873669577/infocenter?_t_=0.6394281481248416")
        listConfig.params = listParmas
        listConfig.setHeader("cookie", cookie)
        listConfig.referer = "https://user.qzone.qq.com/1873669577/infocenter?_t_=0.6394281481248416"
        listConfig.userAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36"
        listConfig.isAjax = true
        val re = Http.post(listConfig)
        val result = String(re)

        println("delete=" + result)
        if (result.contains("\"code\":0,\"err\":{\"code\":0}")) {
            println("删除成功")
        } else {
            println("删除失败")
            throw Exception("删除失败，需要等待一会儿")
        }
    }

}