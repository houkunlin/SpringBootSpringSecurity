import cn.goour.utils.http.Http
import cn.goour.utils.http.HttpConfig
import cn.goour.utils.http.Params
import org.json.JSONObject
import org.junit.Test

class FanLaJiTest {
    @Test
    @Throws(Exception::class)
    fun testFanLaJi() {
        val list = arrayOf(
                "使用假、币 再加上我的自定义词汇",
                "震荡行情，散户把握难，个股机会一直不缺，++散户++葳兴:FT⑤①⑥⑧",
                "网络安全是指网络系统的硬件、软件及其系统中的数据受到保护，不因偶然的或者恶意的原因而遭受到破坏、更改、泄露，系统连续可靠正常地运行，网络服务不中断。",
                "真性感，又射了，射了好多精液到你的照片上",
                "学生党们注意啦！！只要有一部手机！何时何地时间自由都可赚钱！我可以为你推荐好几种工作噢！淘宝好评师也可以！趁暑假自己买点喜欢的东西吧！想了解的资询我噢！",
                "任何一个用户可以上传原创内容的网站、App、直播平台等载体，就是UGC产品。因为内容是网民自主上传，所以存在着各种各样的运营风险。",
                "约炮，姐姐阿姨和少妇满意 ，不影响家庭，做得私聊谈，喜欢被亲吻，被口活的姐姐加我，本人。可长期相处，也可就一次高兴。单身寂寞的，经常空虚的，老公无法满足你的，就想让男人跪下给你舔，泄压满足心理的等等，只要你愿意都可以私聊我。",
                "你好美女！请问一下每晚上做爱两次，每次40分钟以上，但都达不到高潮，是什么情况啊",
                "生活需要细酌慢品，默默揣摩，一笔一画，都在一双手，和一双眼眸。如若在晨露的晶莹剔透中，潺潺清泉脉脉里，浅淡了琉璃白，望过千万风景，一月一云，一叶一花，都是透着纯粹，露着浅淡，那是向往的。世事纷杂在于心，或许无法改变他人，那改变自己。不随波逐流，不自怨自艾，人生的独特之处，是人在山处，心在山里，看云是云，读水是水，做最真的自己，去闲庭一园的彩。",
                "专业提取公积金，无风险，提取速度快，保证账户不清零不冻结，不成功不收费，真实材料，正规手续，保证您放心，满意，电话52962221"
        )
        list.forEach {
            getFan(it)
            Thread.sleep(3000)
        }
    }

    fun getFan(str: String) {
        val config = HttpConfig("http://www.hoapi.com/index.php/Home/Api/check?token=4e16fb6e390bd3d5a7420ad0c274442b")
        val params = Params()
        params.add("str",
                str)
        config.setParams(params)
        val re = Http.post(config)
        val result = String(re)
        val json = JSONObject(result)
//        println(result)
        println(str)
        println(json)
        println()
    }
}