/*
 * Copyright (c) $year.
 * 网址：http://goour.cn
 * 作者：侯坤林
 * 邮箱：lscode@qq.com
 * 侯坤林 版权所有
 *
 */

import cn.goour.utils.http.Http
import cn.goour.utils.http.HttpConfig
import cn.goour.utils.http.Params
import org.json.JSONObject
import org.junit.Test

class PingLunTest {
    val token = "4c1561c5c6c8d1f8c46a7e058caecc117ffbe43caab056c6bfd807a3ce493b0283d963b2d51a67e1b5"
    val g_tk = "1038246997"
    val cookie = "pgv_pvi=5046762496; RK=4dU7ung/a2; tvfe_boss_uuid=7da03e05cc7943e2; o_cookie=1184511588; pac_uid=1_1184511588; __Q_w_s__QZN_TodoMsgCnt=1; __Q_w_s_hat_seed=1; randomSeed=790045; pgv_pvid=2588145936; _ga=GA1.2.48479048.1507560040; _gid=GA1.2.345148191.1510670468; cpu_performance_v8=32; pgv_si=s7986454528; _qpsvr_localtk=0.25671574779215267; pgv_info=ssid=s8768021680; ptui_loginuin=1873669577; ptisp=ctc; ptcz=f71ebb2242313bc0e516652f5b6d339a775c20d80fa25ce718ae15402d4a8226; uin=o1873669577; skey=@Lg5UMsIGM; pt2gguin=o1873669577; p_uin=o1873669577; pt4_token=fchbHrLeVjn2cnSjpuc-1gzpSoTFLyQPU3w1r4kfzlk_; p_skey=njTxZz5K*v6NTv6839O4loyVQP7khR5rndddA1dhi4k_; Loading=Yes; x-stgw-ssl-info=55c22ec0fbeaf0c24689014d8f9ba4bc|0.101|1510754775.020|2|.|I|TLSv1.2|ECDHE-RSA-AES128-GCM-SHA256|31500|h2|0; qz_screen=1920x1080; QZ_FE_WEBP_SUPPORT=1"
    @Test
    @Throws(Exception::class)
    fun testDelete() {

        var ok = false
        while (true) {
            val re = getPage(1)
            val json = JSONObject(re)
            val msglist = json.getJSONObject("data").getJSONArray("commentList")
            val len = msglist.length()
            var j = 0
            while (j < len) {
                val item = msglist.getJSONObject(j++)
                println("item=" + item)
                delete(item.getString("id"), item.getString("uin"))
                Thread.sleep(2000L)
                println()
            }
            println()
//            break
            if (len == 0) {
                ok = true
            }
            if (ok) {
                break
            }
        }
    }

    fun getPage(page: Int): String {
        val listConfig = HttpConfig("https://user.qzone.qq.com/proxy/domain/m.qzone.qq.com/cgi-bin/new/get_msgb")
        val listParmas = Params()
        listParmas.add("uin", "1873669577")
        listParmas.add("hostUin", "1873669577")
        listParmas.add("num", "10")
        listParmas.add("start", "10")
        listParmas.add("hostword", "0")
        listParmas.add("essence", 1)
        listParmas.add("r", "0.8973610717114417")
        listParmas.add("iNotice", 0)
        listParmas.add("inCharset", "utf-8")
        listParmas.add("outCharset", "utf-8")
        listParmas.add("ref", "qzone")
        listParmas.add("code_version", "1")
        listParmas.add("format", "json")
        listParmas.add("need_private_comment", "1")
        listParmas.add("g_tk", g_tk)
        listParmas.add("qzonetoken", token)


        listConfig.params = listParmas
        listConfig.setHeader("cookie", cookie)
        listConfig.referer = "https://qzs.qq.com/qzone/app/mood_v6/html/index.html"
        listConfig.userAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36"
        listConfig.isAjax = true

        val re = Http.get(listConfig)
        val result = String(re)
        println("getPage=" + result)
        return result
    }

    @Throws(Exception::class)
    fun delete(id: String, uin: String) {
        val listConfig = HttpConfig("https://h5.qzone.qq.com/proxy/domain/m.qzone.qq.com/cgi-bin/new/del_msgb?qzonetoken=$token&g_tk=$g_tk")
        val listParmas = Params()
        listParmas.add("hostUin", "1873669577")
        listParmas.add("idList", id)
        listParmas.add("code_version", "1")
        listParmas.add("uinList", uin)
        listParmas.add("format", "fs")
        listParmas.add("inCharset", "utf-8")
        listParmas.add("outCharset", "utf-8")
        listParmas.add("ref", "fs")
        listParmas.add("json", "qzone")
        listParmas.add("g_tk", g_tk)
        listParmas.add("qzreferrer", "https://qzs.qq.com/qzone/msgboard/msgbcanvas.html#page=1")

        listConfig.params = listParmas
        listConfig.setHeader("cookie", cookie)
        listConfig.referer = "https://user.qzone.qq.com/1873669577/infocenter?_t_=0.6394281481248416"
        listConfig.userAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36"
        listConfig.isAjax = true
        val re = Http.post(listConfig)
        val result = String(re)

        if (result.contains("成功删除1条留言")) {
            println("删除成功")
        } else {
            println("删除失败")
            throw Exception("删除失败，需要等待一会儿")
        }
        println("delete=" + result)
    }
}