
import cn.goour.web.App
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.core.io.FileSystemResource
import org.springframework.mail.SimpleMailMessage
import org.springframework.mail.javamail.JavaMailSender
import org.springframework.mail.javamail.MimeMessageHelper
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner
import org.springframework.test.context.web.WebAppConfiguration
import java.io.File
import javax.mail.internet.MimeMessage


/*
 * Copyright (c) $year.
 * 网址：http://goour.cn
 * 作者：侯坤林
 * 邮箱：lscode@qq.com
 * 侯坤林 版权所有
 *
 */

@RunWith(SpringJUnit4ClassRunner::class)
@WebAppConfiguration
@SpringBootTest(classes = arrayOf(App::class))
open class MailTest {
    @Autowired
    lateinit var mailSender: JavaMailSender //自动注入的Bean

    @Value("\${spring.mail.username}")
    lateinit var Sender: String //读取配置文件中的参数

    @Test
    @Throws(Exception::class)
    fun sendSimpleMail() {
        val message = SimpleMailMessage()
        message.from = Sender
        message.setTo(Sender) //自己给自己发送邮件
        message.subject = "主题：简单邮件"
        message.text = "测试邮件内容"
        mailSender.send(message)
    }

    @Test
    fun sendHtmlMail() {
        var message: MimeMessage? = null
        try {
            message = mailSender.createMimeMessage()
            val helper = MimeMessageHelper(message, true)
            helper.setFrom(Sender)
            helper.setTo(Sender)
            helper.setSubject("标题：发送Html内容")

            val sb = StringBuffer()
            sb.append("<h1>大标题-h1</h1>")
                    .append("<p style='color:#F00'>红色字</p>")
                    .append("<p style='text-align:right'>右对齐</p>")
            helper.setText(sb.toString(), true)
        } catch (e: Exception) {
            e.printStackTrace()
        }

        mailSender.send(message)
    }

    @Test
    fun sendAttachmentsMail() {
        var message: MimeMessage? = null
        try {
            message = mailSender.createMimeMessage()
            val helper = MimeMessageHelper(message, true)
            helper.setFrom(Sender)
            helper.setTo(Sender)
            helper.setSubject("主题：带附件的邮件")
            helper.setText("带附件的邮件内容")
            //注意项目路径问题，自动补用项目路径
            val file = FileSystemResource(File("src/main/resources/static/image/picture.jpg"))
            //加入邮件
            helper.addAttachment("图片.jpg", file)
        } catch (e: Exception) {
            e.printStackTrace()
        }

        mailSender.send(message)
    }

    @Test
    fun sendInlineMail() {
        var message: MimeMessage? = null
        try {
            message = mailSender.createMimeMessage()
            val helper = MimeMessageHelper(message, true)
            helper.setFrom(Sender)
            helper.setTo(Sender)
            helper.setSubject("主题：带静态资源的邮件")
            //第二个参数指定发送的是HTML格式,同时cid:是固定的写法
            helper.setText("<html><body>带静态资源的邮件内容 图片:<img src='cid:picture' /></body></html>", true)

            val file = FileSystemResource(File("src/main/resources/static/image/picture.jpg"))
            helper.addInline("picture", file)
        } catch (e: Exception) {
            e.printStackTrace()
        }

        mailSender.send(message)
    }
/*
    @Autowired
    lateinit var freeMarkerConfigurer: FreeMarkerConfigurer  //自动注入

    @Test
    fun sendTemplateMail() {
        var message: MimeMessage? = null
        try {
            message = mailSender.createMimeMessage()
            val helper = MimeMessageHelper(message, true)
            helper.setFrom(Sender)
            helper.setTo(Sender)
            helper.setSubject("主题：模板邮件")

            val model = HashedMap()
            model.put("username", "zggdczfr")

            //修改 application.properties 文件中的读取路径
            //            FreeMarkerConfigurer configurer = new FreeMarkerConfigurer();
            //            configurer.setTemplateLoaderPath("classpath:templates");
            //读取 html 模板
            val template = freeMarkerConfigurer.configuration.getTemplate("mail.html")
            val html = FreeMarkerTemplateUtils.processTemplateIntoString(template, model)
            helper.setText(html, true)
        } catch (e: exception) {
            e.printStackTrace()
        }

        mailSender.send(message)
    }*/
}