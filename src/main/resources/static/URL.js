/*
 * Copyright (c) 2017.
 * 网址：http://goour.cn
 * 作者：侯坤林
 * 邮箱：lscode@qq.com
 * 侯坤林 版权所有
 *
 */

var contextPath = "/LsxyXfBaoXiu";
var kz = ".json";
contextPath = "/lsxyxf";
kz = "";
var AjaxUrl = {
    admin: {
        login: contextPath + "/adminLogin" + kz,

        list: contextPath + "/admin/List" + kz,
        del: contextPath + "/admin/Del" + kz,
        add: contextPath + "/admin/Add" + kz,
        user: {
            list: contextPath + "/admin/user/List" + kz,
            add: contextPath + "/admin/user/Add" + kz,
            del: contextPath + "/admin/user/Del" + kz,
            setLogin: contextPath + "/admin/user/setLogin" + kz,

            import: contextPath + "/admin/user/importUser" + kz,
            export: contextPath + "/admin/user/exportUser" + kz,
            clear: contextPath + "/admin/user/clearUser" + kz,

            update: contextPath + "/admin/user/Update" + kz,
            /*updateName: contextPath + "/admin/user/updateName" + kz,
            updatePhone: contextPath + "/admin/user/updatePhone" + kz,
            updateEmail: contextPath + "/admin/user/updateEmail" + kz,
            updateRoom: contextPath + "/admin/user/updateRoom" + kz,*/
            updatePassword: contextPath + "/admin/user/updatePassword" + kz,
        },
        logging: {
            list: contextPath + "/admin/logging/List" + kz,
        },
        repair: {
            list: contextPath + "/admin/repair/List" + kz,
            setStatus: contextPath + "/admin/repair/setStatus" + kz
        },
        repairClassify: {
            list: contextPath + "/admin/repairClassify/List" + kz,
            add: contextPath + "/admin/repairClassify/Add" + kz,
            update: contextPath + "/admin/repairClassify/Update" + kz,
            delete: contextPath + "/admin/repairClassify/Del" + kz,
        },
        repairSubClassify: {
            list: contextPath + "/admin/repairSubClassify/List" + kz,
            add: contextPath + "/admin/repairSubClassify/Add" + kz,
            update: contextPath + "/admin/repairSubClassify/Update" + kz,
            del: contextPath + "/admin/repairSubClassify/Del" + kz,
        },
        schoolIp: {
            list: contextPath + "/admin/schoolIp/List" + kz,
            add: contextPath + "/admin/schoolIp/Add" + kz,
            update: contextPath + "/admin/schoolIp/Update" + kz,
            del: contextPath + "/admin/schoolIp/Del" + kz,
            export: contextPath + "/admin/schoolIp/export" + kz,
            import: contextPath + "/admin/schoolIp/import" + kz,
            clear: contextPath + "/admin/schoolIp/clear" + kz,
        }
    }
};