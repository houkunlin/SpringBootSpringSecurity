var $parentNode = window.parent.document;

function $childNode(name) {
    return window.frames[name]
}

// tooltips
$('.tooltip-demo').tooltip({
    selector: "[data-toggle=tooltip]",
    container: "body"
});

// 使用animation.css修改Bootstrap Modal
$('.modal').appendTo("body");

$("[data-toggle=popover]").popover();

//折叠ibox
$('.collapse-link').click(function () {
    var ibox = $(this).closest('div.ibox');
    var button = $(this).find('i');
    var content = ibox.find('div.ibox-content');
    content.slideToggle(200);
    button.toggleClass('fa-chevron-up').toggleClass('fa-chevron-down');
    ibox.toggleClass('').toggleClass('border-bottom');
    setTimeout(function () {
        ibox.resize();
        ibox.find('[id^=map-]').resize();
    }, 50);
});

//关闭ibox
$('.close-link').click(function () {
    var content = $(this).closest('div.ibox');
    content.remove();
});

//判断当前页面是否在iframe中
if (top == this) {
    var gohome = '<div class="gohome"><a class="animated bounceInUp" href="index.html?v=4.0" title="返回首页"><i class="fa fa-home"></i></a></div>';
    $('body').append(gohome);
}

//animation.css
function animationHover(element, animation) {
    element = $(element);
    element.hover(
        function () {
            element.addClass('animated ' + animation);
        },
        function () {
            //动画完成之前移除class
            window.setTimeout(function () {
                element.removeClass('animated ' + animation);
            }, 2000);
        });
}

//拖动面板
function WinMove() {
    var element = "[class*=col]";
    var handle = ".ibox-title";
    var connect = "[class*=col]";
    $(element).sortable({
            handle: handle,
            connectWith: connect,
            tolerance: 'pointer',
            forcePlaceholderSize: true,
            opacity: 0.8,
        })
        .disableSelection();
};

function sendAjaxData(url, data, backFn, errorFn, uploadFn, loading) {
	var loading = loading ? loading : null;
	if(layer && loading == null) {
		loading = layer.load(3, {
			shade: [0.3, '#000']
		});
	}
	if(!url) {
		return false;
	}
	var data = data ? data : "";
	var backFn = backFn ? backFn : function(data) {};
	var errorFn = errorFn ? errorFn : function(XMLHttpRequest, textStatus, errorThrown) {};
	var uploadFn1 = uploadFn ? uploadFn : function(e) {
		e.loaded;
		e.total;
	};

	$.ajax({
		url: url,
		type: 'POST',
		dataType: "json",
		data: data,
		async: true,
		cache: false,
		processData: uploadFn ? false : true,
		contentType: uploadFn ? false : "application/x-www-form-urlencoded",
		timeout: 30000,
		xhr: function() {
			myXhr = $.ajaxSettings.xhr();
			if(myXhr.upload) {
				myXhr.upload.addEventListener('progress', uploadFn1, false); // for handling the progress of the upload
			}
			return myXhr;
		},
		beforeSend: function() {},
		success: function(data) {
			if(layer) {
				layer.close(loading);
			}
			if(data.url && data.url != null) {
				layer.alert(data.msg, function(index) {
					window.location.href = data.url;
					layer.close(index);
				});
			} else {
				backFn(data);
			}
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			if(layer) {
				layer.close(loading);
			}
			errorFn(XMLHttpRequest, textStatus, errorThrown);
		}
	});
}
function getQueryString(name) {
    var reg = new RegExp('(^|&)' + name + '=([^&]*)(&|$)', 'i');
    var r = window.location.search.substr(1).match(reg);
    if (r != null) {
        return unescape(r[2]);
    }
    return null;
}