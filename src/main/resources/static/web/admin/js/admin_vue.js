layui.use(['layer', 'laypage'], function () {
    var app = new Vue({
        el: '#body',
        data: {
            my: {},
            selectAll: false,
            list: [],
            pageInfo: {
                page: 1,
                size: 10,
                search: ""
            },
            info: {}
        },
        methods: {
            search: function () {
                this.loadPage(1);
            },
            add: function () {
                var index = layui.layer.open({
                    type: 2,
                    title: '系统管理员详细信息',
                    // shadeClose: true,
                    // shade: false,
                    maxmin: true, //开启最大化最小化按钮
                    area: ['893px', '600px'],
                    content: 'adminInfo.html'
                });
                // layui.layer.full(index);
            },
            update: function (item) {
                var index = layui.layer.open({
                    type: 2,
                    title: '系统管理员详细信息',
                    // shadeClose: true,
                    // shade: false,
                    maxmin: true, //开启最大化最小化按钮
                    area: ['893px', '600px'],
                    content: 'adminInfo.html'
                });
            },
            del: function (item) {
                console.log(item);
                var vue = this;
                var data = {id: []};
                var names = [];
                if (item === undefined) {
                    for (var i in this.list) {
                        var item1 = this.list[i];
                        if (item1.select) {
                            data.id.push(item1.id);
                            names.push(item1.name);
                        }
                    }
                } else {
                    data.id.push(item.id);
                    names.push(item.name);
                }
                if (names.length === 0) {
                    layui.layer.msg("没有选择内容", {time: 20000, btn: ['明白了']});
                    return false;
                }
                layui.layer.confirm('您要删除【' + names.join("、") + '】吗？', {
                    btn: ['确定删除', '不，留着生孩子'] //按钮
                }, function () {
                    layui.layer.prompt({
                        formType: 1,
                        value: "",
                        title: '请验证当前用户密码'
                    }, function (value, index, elem) {
                        layui.layer.close(index);
                        data.password = value;
                        data._method = "DELETE";
                        $.ajax({
                            type: "post",
                            data: data,
                            url: "",
                            async: true,
                            dataType: "json",
                            success: function (data) {
                                if (data.code === 0) {
                                    vue.loadPage(1);
                                } else {
                                    layui.layer.msg(data.msg, {time: 20000, btn: ['明白了']}, function () {
                                        vue.loadPage(1);
                                    });
                                }
                            },
                            error: function () {
                                layui.layer.msg("加载错误");
                            }
                        });
                    });
                });
            },
            select: function () {
                for (var i = 0; i < this.list.length; i++) {
                    this.list[i].select = this.selectAll;
                }
            },
            loadPage: function (page) {
                var vue = this;
                if (page * 1 < 1) {
                    vue.pageInfo.page = 1;
                } else if (page * 1 > 0) {
                    vue.pageInfo.page = page;
                }
                $.ajax({
                    type: "get",
                    data: vue.pageInfo,
                    url: "",
                    async: true,
                    dataType: "json",
                    success: function (data) {
                        if (data.code === 0) {
                            layui.layer.msg(data.msg);
                            vue.my = data.bean;
                            vue.list = data.list;
                            layui.laypage.render({
                                elem: "page",
                                count: data.count,
                                limit: vue.pageInfo.size,
                                curr: data.pageCurr,
                                groups: 10,
                                jump: function (obj, first) {
                                    //得到了当前页，用于向服务端请求对应数据
                                    var curr = obj.curr;
                                    if (!first) { //点击跳页触发函数自身，并传递当前页：obj.curr
                                        vue.loadPage(curr);
                                    }
                                }
                            });
                        } else {
                            vue.list = [];
                            layui.layer.msg(data.msg, {time: 20000, btn: ['明白了']});
                        }
                    },
                    error: function () {
                        vue.list = [];
                        layui.layer.msg("加载错误", {time: 5000, btn: ['明白了']});
                    }
                });
            }
        },
        mounted: function () {
            this.loadPage(1);
        }
    });
});