/*
 * Copyright (c) 2017.
 */

package com.qq.weixin.open.entity;

import java.util.ArrayList;
import java.util.List;
import com.alibaba.fastjson.JSONArray;

public class ParseJsonArray {
    public static List<String> parseStringList(JSONArray jsonArray) {
        List<String> list = new ArrayList<String>();
        if (jsonArray != null) {
            for (Object object : jsonArray) {
                list.add(object.toString());
            }

        }

        return list;
    }

    public static List<Integer> parseIntegerList(JSONArray jsonArray) {
        List<Integer> list = new ArrayList<Integer>();
        if (jsonArray != null) {
            for (Object object : jsonArray) {
                list.add((Integer) object);
            }

        }

        return list;
    }

    public static List<Long> parseLongList(JSONArray jsonArray) {
        List<Long> list = new ArrayList<Long>();
        if (jsonArray != null) {
            for (Object object : jsonArray) {
                list.add((Long) object);
            }

        }

        return list;
    }

    public static List<Float> parseFloatList(JSONArray jsonArray) {
        List<Float> list = new ArrayList<Float>();
        if (jsonArray != null) {
            for (Object object : jsonArray) {
                list.add((Float) object);
            }

        }

        return list;
    }

    public static List<Double> parseDoubleList(JSONArray jsonArray) {
        List<Double> list = new ArrayList<Double>();
        if (jsonArray != null) {
            for (Object object : jsonArray) {
                list.add((Double) object);
            }

        }

        return list;
    }

    public static List<Boolean> parseBooleanList(JSONArray jsonArray) {
        List<Boolean> list = new ArrayList<Boolean>();
        if (jsonArray != null) {
            for (Object object : jsonArray) {
                list.add((Boolean) object);
            }

        }

        return list;
    }

    public static List<Object> parseObjectList(JSONArray jsonArray) {
        List<Object> list = new ArrayList<Object>();
        if (jsonArray != null) {
            for (Object object : jsonArray) {
                list.add(object);
            }

        }

        return list;
    }
}
