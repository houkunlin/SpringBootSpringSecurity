/*
 * Copyright (c) 2017.
 * 网址：http://goour.cn
 * 作者：侯坤林
 * 邮箱：lscode@qq.com
 * 侯坤林 版权所有
 *
 */

package com.qq.weixin.open.entity;

import java.io.Serializable;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.JSONArray;
import java.util.List;
import java.util.ArrayList;

/**
 * 2017-10-15 19:17:36
 * @author HouKunLin
 * 
 */
public class AccessToken implements Serializable {
    private static final long serialVersionUID = 1L;
    private String access_token;
    private int errcode;
    private String refresh_token;
    private String unionid;
    private String openid;
    private String scope;
    private String errmsg;
    private int expires_in;
    
    public AccessToken() {
    }

    public String getAccess_token() {
        return access_token;
    }

    public int getErrcode() {
        return errcode;
    }

    public String getRefresh_token() {
        return refresh_token;
    }

    public String getUnionid() {
        return unionid;
    }

    public String getOpenid() {
        return openid;
    }

    public String getScope() {
        return scope;
    }

    public String getErrmsg() {
        return errmsg;
    }

    public int getExpires_in() {
        return expires_in;
    }

    public void setAccess_token(String access_token) {
        this.access_token=access_token;
    }

    public void setErrcode(int errcode) {
        this.errcode=errcode;
    }

    public void setRefresh_token(String refresh_token) {
        this.refresh_token=refresh_token;
    }

    public void setUnionid(String unionid) {
        this.unionid=unionid;
    }

    public void setOpenid(String openid) {
        this.openid=openid;
    }

    public void setScope(String scope) {
        this.scope=scope;
    }

    public void setErrmsg(String errmsg) {
        this.errmsg=errmsg;
    }

    public void setExpires_in(int expires_in) {
        this.expires_in=expires_in;
    }

    public static AccessToken parseJsonObject(JSONObject jsonObject) {
        AccessToken vo = new AccessToken();
        if(jsonObject == null) {
            return vo;
        }

        if (jsonObject.containsKey("access_token")) {
            vo.setAccess_token(jsonObject.getString("access_token"));
        }

        if (jsonObject.containsKey("errcode")) {
            vo.setErrcode(jsonObject.getIntValue("errcode"));
        }

        if (jsonObject.containsKey("refresh_token")) {
            vo.setRefresh_token(jsonObject.getString("refresh_token"));
        }

        if (jsonObject.containsKey("unionid")) {
            vo.setUnionid(jsonObject.getString("unionid"));
        }

        if (jsonObject.containsKey("openid")) {
            vo.setOpenid(jsonObject.getString("openid"));
        }

        if (jsonObject.containsKey("scope")) {
            vo.setScope(jsonObject.getString("scope"));
        }

        if (jsonObject.containsKey("errmsg")) {
            vo.setErrmsg(jsonObject.getString("errmsg"));
        }

        if (jsonObject.containsKey("expires_in")) {
            vo.setExpires_in(jsonObject.getIntValue("expires_in"));
        }

        return vo;
    }

    public static List<AccessToken> parseJsonArray(JSONArray jsonArray) {
        List<AccessToken> list = new ArrayList<AccessToken>();
        if(jsonArray == null) {
            return list;
        }

        for(Object object:jsonArray) {
            JSONObject item = (JSONObject)object;
            list.add(parseJsonObject(item));
        }

        return list;
    }

    public String toString() {
        return "AccessToken [ access_token=" + access_token + ",errcode=" + errcode + ",refresh_token=" + refresh_token + ",unionid=" + unionid + ",openid=" + openid + ",scope=" + scope + ",errmsg=" + errmsg + ",expires_in=" + expires_in + "]";
    }
}
