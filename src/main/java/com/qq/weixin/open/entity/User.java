/*
 * Copyright (c) 2017.
 * ��ַ��http://goour.cn
 * ���ߣ�������
 * ���䣺lscode@qq.com
 * ������ ��Ȩ����
 */

package com.qq.weixin.open.entity;

import java.io.Serializable;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.JSONArray;
import java.util.List;
import java.util.ArrayList;

/**
 * 2017-10-15 18:54:24
 * @author HouKunLin
 * 
 */
public class User implements Serializable {
    private static final long serialVersionUID = 1L;
    private int errcode;
    private String country;
    private String unionid;// 当且仅当该网站应用已获得该用户的userinfo授权时，才会出现该字段。
    private String province;
    private String city;
    private String openid;
    private String sex;
    private String nickname;
    private String headimgurl;
    private String errmsg;
    private List<String> privilege;
    
    public User() {
    }

    public int getErrcode() {
        return errcode;
    }

    public String getCountry() {
        return country;
    }

    public String getUnionid() {
        return unionid;
    }

    public String getProvince() {
        return province;
    }

    public String getCity() {
        return city;
    }

    public String getOpenid() {
        return openid;
    }

    public String getSex() {
        return sex;
    }

    public String getNickname() {
        return nickname;
    }

    public String getHeadimgurl() {
        return headimgurl;
    }

    public String getErrmsg() {
        return errmsg;
    }

    public List<String> getPrivilege() {
        return privilege;
    }

    public void setErrcode(int errcode) {
        this.errcode=errcode;
    }

    public void setCountry(String country) {
        this.country=country;
    }

    public void setUnionid(String unionid) {
        this.unionid=unionid;
    }

    public void setProvince(String province) {
        this.province=province;
    }

    public void setCity(String city) {
        this.city=city;
    }

    public void setOpenid(String openid) {
        this.openid=openid;
    }

    public void setSex(String sex) {
        this.sex=sex;
    }

    public void setNickname(String nickname) {
        this.nickname=nickname;
    }

    public void setHeadimgurl(String headimgurl) {
        this.headimgurl=headimgurl;
    }

    public void setErrmsg(String errmsg) {
        this.errmsg=errmsg;
    }

    public void setPrivilege(List<String> privilege) {
        this.privilege=privilege;
    }

    public static User parseJsonObject(JSONObject jsonObject) {
        User vo = new User();
        if(jsonObject == null) {
            return vo;
        }

        if (jsonObject.containsKey("errcode")) {
            vo.setErrcode(jsonObject.getIntValue("errcode"));
        }

        if (jsonObject.containsKey("country")) {
            vo.setCountry(jsonObject.getString("country"));
        }

        if (jsonObject.containsKey("unionid")) {
            vo.setUnionid(jsonObject.getString("unionid"));
        }

        if (jsonObject.containsKey("province")) {
            vo.setProvince(jsonObject.getString("province"));
        }

        if (jsonObject.containsKey("city")) {
            vo.setCity(jsonObject.getString("city"));
        }

        if (jsonObject.containsKey("openid")) {
            vo.setOpenid(jsonObject.getString("openid"));
        }

        if (jsonObject.containsKey("sex")) {
            vo.setSex(jsonObject.getString("sex"));
        }

        if (jsonObject.containsKey("nickname")) {
            vo.setNickname(jsonObject.getString("nickname"));
        }

        if (jsonObject.containsKey("headimgurl")) {
            vo.setHeadimgurl(jsonObject.getString("headimgurl"));
        }

        if (jsonObject.containsKey("errmsg")) {
            vo.setErrmsg(jsonObject.getString("errmsg"));
        }

        if (jsonObject.containsKey("privilege")) {
            vo.setPrivilege(ParseJsonArray.parseStringList(jsonObject.getJSONArray("privilege")));
        }

        return vo;
    }

    public static List<User> parseJsonArray(JSONArray jsonArray) {
        List<User> list = new ArrayList<User>();
        if(jsonArray == null) {
            return list;
        }

        for(Object object:jsonArray) {
            JSONObject item = (JSONObject)object;
            list.add(parseJsonObject(item));
        }

        return list;
    }

    public String toString() {
        return "User [ errcode=" + errcode + ",country=" + country + ",unionid=" + unionid + ",province=" + province + ",city=" + city + ",openid=" + openid + ",sex=" + sex + ",nickname=" + nickname + ",headimgurl=" + headimgurl + ",errmsg=" + errmsg + ",privilege=" + privilege + "]";
    }
}
