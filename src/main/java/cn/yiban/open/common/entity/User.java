/*
 * Copyright (c) 2017.
 * ��ַ��http://goour.cn
 * ���ߣ�������
 * ���䣺lscode@qq.com
 * ������ ��Ȩ����
 */

package cn.yiban.open.common.entity;

import java.io.Serializable;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.JSONArray;
import java.util.List;
import java.util.ArrayList;

/**
 * 2017-10-13 23:02:22
 * @author HouKunLin
 * 
 */
public class User implements Serializable {
    private static final long serialVersionUID = 1L;
    private UserInfo userInfo;
    private String status;
    
    public User() {
    }

    public UserInfo getUserInfo() {
        return userInfo;
    }

    public String getStatus() {
        return status;
    }

    public void setUserInfo(UserInfo userInfo) {
        this.userInfo=userInfo;
    }

    public void setStatus(String status) {
        this.status=status;
    }

    public static User parseJsonObject(JSONObject jsonObject) {
        User vo = new User();
        if(jsonObject == null) {
            return vo;
        }

        if (jsonObject.containsKey("info")) {
            vo.setUserInfo(UserInfo.parseJsonObject(jsonObject.getJSONObject("info")));
        }

        if (jsonObject.containsKey("status")) {
            vo.setStatus(jsonObject.getString("status"));
        }

        return vo;
    }

    public static List<User> parseJsonArray(JSONArray jsonArray) {
        List<User> list = new ArrayList<User>();
        if(jsonArray == null) {
            return list;
        }

        for(Object object:jsonArray) {
            JSONObject item = (JSONObject)object;
            list.add(parseJsonObject(item));
        }

        return list;
    }

    public String toString() {
        return "User [ userInfo=" + userInfo + ",status=" + status + "]";
    }
}
