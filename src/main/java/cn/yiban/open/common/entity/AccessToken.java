/*
 * Copyright (c) 2017.
 * 网址：http://goour.cn
 * 作者：侯坤林
 * 邮箱：lscode@qq.com
 * 侯坤林 版权所有
 *
 */

package cn.yiban.open.common.entity;

import java.io.Serializable;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.JSONArray;
import java.util.List;
import java.util.ArrayList;

/**
 * 2017-10-13 19:42:38
 * @author HouKunLin
 * 
 */
public class AccessToken implements Serializable {
    private static final long serialVersionUID = 1L;
    private String access_token;
    private AccessTokenInfo accessTokenInfo;
    private String expires;
    private String userid;
    private String status;
    
    public AccessToken() {
    }

    public String getAccess_token() {
        return access_token;
    }

    public AccessTokenInfo getAccessTokenInfo() {
        return accessTokenInfo;
    }

    public String getExpires() {
        return expires;
    }

    public String getUserid() {
        return userid;
    }

    public String getStatus() {
        return status;
    }

    public void setAccess_token(String access_token) {
        this.access_token=access_token;
    }

    public void setAccessTokenInfo(AccessTokenInfo accessTokenInfo) {
        this.accessTokenInfo=accessTokenInfo;
    }

    public void setExpires(String expires) {
        this.expires=expires;
    }

    public void setUserid(String userid) {
        this.userid=userid;
    }

    public void setStatus(String status) {
        this.status=status;
    }

    public static AccessToken parseJsonObject(JSONObject jsonObject) {
        AccessToken vo = new AccessToken();
        if(jsonObject == null) {
            return vo;
        }

        if (jsonObject.containsKey("access_token")) {
            vo.setAccess_token(jsonObject.getString("access_token"));
        }

        if (jsonObject.containsKey("info")) {
            vo.setAccessTokenInfo(AccessTokenInfo.parseJsonObject(jsonObject.getJSONObject("info")));
        }

        if (jsonObject.containsKey("expires")) {
            vo.setExpires(jsonObject.getString("expires"));
        }

        if (jsonObject.containsKey("userid")) {
            vo.setUserid(jsonObject.getString("userid"));
        }

        if (jsonObject.containsKey("status")) {
            vo.setStatus(jsonObject.getString("status"));
        }

        return vo;
    }

    public static List<AccessToken> parseJsonArray(JSONArray jsonArray) {
        List<AccessToken> list = new ArrayList<AccessToken>();
        if(jsonArray == null) {
            return list;
        }

        for(Object object:jsonArray) {
            JSONObject item = (JSONObject)object;
            list.add(parseJsonObject(item));
        }

        return list;
    }

    public String toString() {
        return "AccessToken [ access_token=" + access_token + ",accessTokenInfo=" + accessTokenInfo + ",expires=" + expires + ",userid=" + userid + ",status=" + status + "]";
    }
}
