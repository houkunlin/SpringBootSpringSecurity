/*
 * Copyright (c) 2017.
 * 网址：http://goour.cn
 * 作者：侯坤林
 * 邮箱：lscode@qq.com
 * 侯坤林 版权所有
 *
 */

package cn.yiban.open.common.entity;

import java.io.Serializable;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.JSONArray;
import java.util.List;
import java.util.ArrayList;

/**
 * 2017-10-13 19:42:38
 * @author HouKunLin
 * 
 */
public class AccessTokenInfo implements Serializable {
    private static final long serialVersionUID = 1L;
    private String code;
    private String msgCN;
    private String msgEN;
    
    public AccessTokenInfo() {
    }

    public String getCode() {
        return code;
    }

    public String getMsgCN() {
        return msgCN;
    }

    public String getMsgEN() {
        return msgEN;
    }

    public void setCode(String code) {
        this.code=code;
    }

    public void setMsgCN(String msgCN) {
        this.msgCN=msgCN;
    }

    public void setMsgEN(String msgEN) {
        this.msgEN=msgEN;
    }

    public static AccessTokenInfo parseJsonObject(JSONObject jsonObject) {
        AccessTokenInfo vo = new AccessTokenInfo();
        if(jsonObject == null) {
            return vo;
        }

        if (jsonObject.containsKey("code")) {
            vo.setCode(jsonObject.getString("code"));
        }

        if (jsonObject.containsKey("msgCN")) {
            vo.setMsgCN(jsonObject.getString("msgCN"));
        }

        if (jsonObject.containsKey("msgEN")) {
            vo.setMsgEN(jsonObject.getString("msgEN"));
        }

        return vo;
    }

    public static List<AccessTokenInfo> parseJsonArray(JSONArray jsonArray) {
        List<AccessTokenInfo> list = new ArrayList<AccessTokenInfo>();
        if(jsonArray == null) {
            return list;
        }

        for(Object object:jsonArray) {
            JSONObject item = (JSONObject)object;
            list.add(parseJsonObject(item));
        }

        return list;
    }

    public String toString() {
        return "AccessTokenInfo [ code=" + code + ",msgCN=" + msgCN + ",msgEN=" + msgEN + "]";
    }
}
