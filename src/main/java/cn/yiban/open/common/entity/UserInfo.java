/*
 * Copyright (c) 2017.
 * ��ַ��http://goour.cn
 * ���ߣ�������
 * ���䣺lscode@qq.com
 * ������ ��Ȩ����
 */

package cn.yiban.open.common.entity;

import java.io.Serializable;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.JSONArray;
import java.util.List;
import java.util.ArrayList;

/**
 * 2017-10-13 23:02:22
 * @author HouKunLin
 * 
 */
public class UserInfo implements Serializable {
    private static final long serialVersionUID = 1L;
    private String yb_usernick;
    private String yb_userhead;
    private String code;
    private String yb_username;
    private String yb_schoolname;
    private String yb_money;
    private String yb_exp;
    private String yb_birthday;
    private String yb_studentid;
    private String yb_schoolid;
    private String yb_identity;
    private String msgCN;
    private String yb_userid;
    private String yb_sex;
    private String yb_realname;
    private String msgEN;
    
    public UserInfo() {
    }

    public String getYb_usernick() {
        return yb_usernick;
    }

    public String getYb_userhead() {
        return yb_userhead;
    }

    public String getCode() {
        return code;
    }

    public String getYb_username() {
        return yb_username;
    }

    public String getYb_schoolname() {
        return yb_schoolname;
    }

    public String getYb_money() {
        return yb_money;
    }

    public String getYb_exp() {
        return yb_exp;
    }

    public String getYb_birthday() {
        return yb_birthday;
    }

    public String getYb_studentid() {
        return yb_studentid;
    }

    public String getYb_schoolid() {
        return yb_schoolid;
    }

    public String getYb_identity() {
        return yb_identity;
    }

    public String getMsgCN() {
        return msgCN;
    }

    public String getYb_userid() {
        return yb_userid;
    }

    public String getYb_sex() {
        return yb_sex;
    }

    public String getYb_realname() {
        return yb_realname;
    }

    public String getMsgEN() {
        return msgEN;
    }

    public void setYb_usernick(String yb_usernick) {
        this.yb_usernick=yb_usernick;
    }

    public void setYb_userhead(String yb_userhead) {
        this.yb_userhead=yb_userhead;
    }

    public void setCode(String code) {
        this.code=code;
    }

    public void setYb_username(String yb_username) {
        this.yb_username=yb_username;
    }

    public void setYb_schoolname(String yb_schoolname) {
        this.yb_schoolname=yb_schoolname;
    }

    public void setYb_money(String yb_money) {
        this.yb_money=yb_money;
    }

    public void setYb_exp(String yb_exp) {
        this.yb_exp=yb_exp;
    }

    public void setYb_birthday(String yb_birthday) {
        this.yb_birthday=yb_birthday;
    }

    public void setYb_studentid(String yb_studentid) {
        this.yb_studentid=yb_studentid;
    }

    public void setYb_schoolid(String yb_schoolid) {
        this.yb_schoolid=yb_schoolid;
    }

    public void setYb_identity(String yb_identity) {
        this.yb_identity=yb_identity;
    }

    public void setMsgCN(String msgCN) {
        this.msgCN=msgCN;
    }

    public void setYb_userid(String yb_userid) {
        this.yb_userid=yb_userid;
    }

    public void setYb_sex(String yb_sex) {
        this.yb_sex=yb_sex;
    }

    public void setYb_realname(String yb_realname) {
        this.yb_realname=yb_realname;
    }

    public void setMsgEN(String msgEN) {
        this.msgEN=msgEN;
    }

    public static UserInfo parseJsonObject(JSONObject jsonObject) {
        UserInfo vo = new UserInfo();
        if(jsonObject == null) {
            return vo;
        }

        if (jsonObject.containsKey("yb_usernick")) {
            vo.setYb_usernick(jsonObject.getString("yb_usernick"));
        }

        if (jsonObject.containsKey("yb_userhead")) {
            vo.setYb_userhead(jsonObject.getString("yb_userhead"));
        }

        if (jsonObject.containsKey("code")) {
            vo.setCode(jsonObject.getString("code"));
        }

        if (jsonObject.containsKey("yb_username")) {
            vo.setYb_username(jsonObject.getString("yb_username"));
        }

        if (jsonObject.containsKey("yb_schoolname")) {
            vo.setYb_schoolname(jsonObject.getString("yb_schoolname"));
        }

        if (jsonObject.containsKey("yb_money")) {
            vo.setYb_money(jsonObject.getString("yb_money"));
        }

        if (jsonObject.containsKey("yb_exp")) {
            vo.setYb_exp(jsonObject.getString("yb_exp"));
        }

        if (jsonObject.containsKey("yb_birthday")) {
            vo.setYb_birthday(jsonObject.getString("yb_birthday"));
        }

        if (jsonObject.containsKey("yb_studentid")) {
            vo.setYb_studentid(jsonObject.getString("yb_studentid"));
        }

        if (jsonObject.containsKey("yb_schoolid")) {
            vo.setYb_schoolid(jsonObject.getString("yb_schoolid"));
        }

        if (jsonObject.containsKey("yb_identity")) {
            vo.setYb_identity(jsonObject.getString("yb_identity"));
        }

        if (jsonObject.containsKey("msgCN")) {
            vo.setMsgCN(jsonObject.getString("msgCN"));
        }

        if (jsonObject.containsKey("yb_userid")) {
            vo.setYb_userid(jsonObject.getString("yb_userid"));
        }

        if (jsonObject.containsKey("yb_sex")) {
            vo.setYb_sex(jsonObject.getString("yb_sex"));
        }

        if (jsonObject.containsKey("yb_realname")) {
            vo.setYb_realname(jsonObject.getString("yb_realname"));
        }

        if (jsonObject.containsKey("msgEN")) {
            vo.setMsgEN(jsonObject.getString("msgEN"));
        }

        return vo;
    }

    public static List<UserInfo> parseJsonArray(JSONArray jsonArray) {
        List<UserInfo> list = new ArrayList<UserInfo>();
        if(jsonArray == null) {
            return list;
        }

        for(Object object:jsonArray) {
            JSONObject item = (JSONObject)object;
            list.add(parseJsonObject(item));
        }

        return list;
    }

    public String toString() {
        return "UserInfo [ yb_usernick=" + yb_usernick + ",yb_userhead=" + yb_userhead + ",code=" + code + ",yb_username=" + yb_username + ",yb_schoolname=" + yb_schoolname + ",yb_money=" + yb_money + ",yb_exp=" + yb_exp + ",yb_birthday=" + yb_birthday + ",yb_studentid=" + yb_studentid + ",yb_schoolid=" + yb_schoolid + ",yb_identity=" + yb_identity + ",msgCN=" + msgCN + ",yb_userid=" + yb_userid + ",yb_sex=" + yb_sex + ",yb_realname=" + yb_realname + ",msgEN=" + msgEN + "]";
    }
}
