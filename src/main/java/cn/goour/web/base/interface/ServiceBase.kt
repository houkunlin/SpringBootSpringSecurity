package cn.goour.web.base.`interface`

interface ServiceBase<T> {
    fun add(vo: T):T
    fun update(vo: T)
    fun delete(ids: Array<Int>)
}