/*
 * Copyright (c) 2017.
 * 网址：http://goour.cn
 * 作者：侯坤林
 * 邮箱：lscode@qq.com
 * 侯坤林 版权所有
 *
 */

import java.text.SimpleDateFormat


private val formatter = SimpleDateFormat()

fun java.util.Date.format(): String {
    return format("yyyy-MM-dd HH:mm:ss")
}

@Synchronized
fun java.util.Date.format(pattern: String): String {
    formatter.applyPattern(pattern)
    return formatter.format(this)
}