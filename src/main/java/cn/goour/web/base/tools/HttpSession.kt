import javax.servlet.http.HttpSession

fun HttpSession.getAttributeToBoolean(key: String): Boolean {
    val obj = this.getAttribute(key)
    if (obj is Boolean) {
        return obj
    }
    return false
}

fun HttpSession.getAttributeToString(key: String): String {
    val obj = this.getAttribute(key)
    if (obj is String) {
        return obj
    }
    return ""
}