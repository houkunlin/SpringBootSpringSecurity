import javax.servlet.ServletContext

fun ServletContext.getAttributeToBoolean(key: String): Boolean {
    val obj = this.getAttribute(key)
    if (obj is Boolean) {
        return obj
    }
    return false
}

fun ServletContext.getAttributeToString(key: String): String {
    val obj = this.getAttribute(key)
    if (obj is String) {
        return obj
    }
    return ""
}