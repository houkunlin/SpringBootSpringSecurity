package cn.goour.web.base.config

import org.springframework.context.annotation.Configuration
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter


@Configuration
open class WebMvcConfig : WebMvcConfigurerAdapter() {
    override fun addViewControllers(registry: ViewControllerRegistry) {
        val admin = "web/admin"
        registry.addViewController("/login").setViewName("login")
//        registry.addViewController("/adminLogin").setViewName("$admin/login")
        registry.addViewController("/admin/login").setViewName("$admin/login")
        registry.addViewController("/admin/index").setViewName("$admin/index")

        registry.addViewController("/admin/settings/admins").setViewName("$admin/settings/admin")

//        registry.addViewController("/admin/settings/admins/add").setViewName("$admin/settings/adminInfo")
//        registry.addViewController("/admin/settings/admins/update_{id}").setViewName("$admin/settings/adminInfo")

        registry.addViewController("/admin/settings/adminLogs").setViewName("$admin/settings/adminLogs")
        registry.addViewController("/admin/settings/adminLogs/admin_{id}").setViewName("$admin/settings/logInfo")

        registry.addViewController("/admin/settings/users").setViewName("$admin/settings/user")
        registry.addViewController("/admin/settings/usersLogs").setViewName("$admin/settings/user")

        registry.addViewController("/admin/settings/setting").setViewName("$admin/settings/setting")
    }

    /*@Bean
    open fun methodValidationPostProcessor(): MethodValidationPostProcessor = MethodValidationPostProcessor()*/
}
