/*package cn.goour.web.base.config


import cn.goour.web.base.handler.AdminLoginFailureHandler
import cn.goour.web.base.handler.AdminLoginSuccessHandler
import cn.goour.web.sys.service.AdminService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter


@Configuration
@EnableWebSecurity(debug = false)
@EnableGlobalMethodSecurity(prePostEnabled = true)
open class WebSecurityConfig : WebSecurityConfigurerAdapter() {

    @Autowired
    lateinit var adminService: AdminService
    @Autowired
    lateinit var adminLoginSuccessHandler: AdminLoginSuccessHandler
    @Autowired
    lateinit var adminLoginFailureHandler: AdminLoginFailureHandler

    @Throws(Exception::class)
    override fun configure(auth: AuthenticationManagerBuilder?) {
        super.configure(auth)
        auth!!.userDetailsService(adminService)
    }

    @Throws(Exception::class)
    override fun configure(http: HttpSecurity) {
        http
                .csrf().disable()
                .headers().disable()
                .authorizeRequests()
                .antMatchers("/admin/**").authenticated()
                .and().formLogin().loginPage("/admin/login").failureForwardUrl("/admin/login?error").successHandler(adminLoginSuccessHandler)
                .failureHandler(adminLoginFailureHandler)
                .permitAll()
                .and().rememberMe().key("springLogin").tokenValiditySeconds(60 * 2)
                .and().logout().logoutUrl("/admin/logout").permitAll()
                .and().anonymous().authorities("ANON")
    }

    @Bean
    @Throws(Exception::class)
    override fun authenticationManagerBean(): AuthenticationManager {
        return super.authenticationManagerBean()
    }
}
*/