package cn.goour.web.base.listener

import cn.goour.web.sys.repository.AdminRepository
import cn.goour.web.sys.repository.LogRepository
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Configuration
import javax.servlet.http.HttpSessionEvent
import javax.servlet.http.HttpSessionListener

//@WebListener
@Configuration
open class HttpSessionListener : HttpSessionListener {
    @Autowired
    lateinit var logRepository: LogRepository

    @Autowired
    lateinit var adminRepository: AdminRepository

    override fun sessionCreated(p0: HttpSessionEvent?) {
        logger.info("一个新的会话$this")
    }

    override fun sessionDestroyed(p0: HttpSessionEvent?) {
        logger.info("Session 会话消失$this")
    }

    companion object {
        private val logger = LoggerFactory.getLogger(HttpSessionListener::class.java)
    }
}