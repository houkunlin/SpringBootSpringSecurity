package cn.goour.web.base.exception

import org.springframework.validation.Errors

/**
 * 返回json数据的访问，在验证bean参数时校验失败
 *
 */
class BackJsonBeanValidFailException : RuntimeException {
    var code: Int = 1
    var msgs: Array<String> = arrayOf()
    override var message = "参数校验失败"

    constructor() : super("参数校验失败") {}
    constructor(message: String) : super(message) {
        this.message = message
    }

    constructor(code: Int, message: String) : this(message) {
        this.code = code
    }

    constructor(code: Int, message: String, msgs: Array<String>) : this(code, message) {
        this.msgs = msgs
    }

    constructor(errors: Errors) : this("请求参数校验失败") {
        val list = arrayListOf<String>()
        errors.allErrors.forEach {
            list.add(it.defaultMessage)
        }
        msgs = list.toTypedArray()
    }

    constructor(code: Int, errors: Errors) : this(errors) {
        this.code = code
    }

}