/*
package cn.goour.web.base.handler

import org.slf4j.LoggerFactory
import org.springframework.context.annotation.Configuration
import org.springframework.security.core.AuthenticationException
import org.springframework.security.web.authentication.ExceptionMappingAuthenticationFailureHandler
import org.springframework.security.web.savedrequest.HttpSessionRequestCache
import org.springframework.security.web.savedrequest.RequestCache
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@Configuration
open class AdminLoginFailureHandler : ExceptionMappingAuthenticationFailureHandler() {

    private var requestCache: RequestCache = HttpSessionRequestCache()

    override fun onAuthenticationFailure(request: HttpServletRequest?, response: HttpServletResponse?, authentication: AuthenticationException?) {
        val obj = authentication!!.localizedMessage
        var url: String? = null
        logger.info(obj)
        // 从别的请求页面跳转过来的情况，savedRequest不为空
        val savedRequest = requestCache.getRequest(request, response)
        if (savedRequest != null) {
            url = savedRequest.redirectUrl
        }
        logger.info("$savedRequest")
        logger.info("$url")

        redirectStrategy.sendRedirect(request, response, "/")
//        super.onAuthenticationSuccess(request, response, authentication)
    }

    companion object {
        private val logger = LoggerFactory.getLogger(AdminLoginFailureHandler::class.java)
    }
}*/
