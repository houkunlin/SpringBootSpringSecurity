/*
package cn.goour.web.base.handler

import cn.goour.web.sys.entity.Admin
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.InitializingBean
import org.springframework.context.annotation.Configuration
import org.springframework.security.core.Authentication
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler
import org.springframework.security.web.savedrequest.HttpSessionRequestCache
import org.springframework.security.web.savedrequest.RequestCache
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse


@Configuration
open class AdminLoginSuccessHandler : SavedRequestAwareAuthenticationSuccessHandler(), InitializingBean {

    private val authDispatcherMap: Map<String, String>? = null
    private var requestCache: RequestCache = HttpSessionRequestCache()

    override fun onAuthenticationSuccess(request: HttpServletRequest?, response: HttpServletResponse?, authentication: Authentication?) {
        val obj = authentication!!.principal
        logger.info("登录成功：$obj")
        logger.info("${authentication.authorities}")
        if (obj is Admin) {
            logger.info("obj is Admin")
        } else {
            logger.info("obj is Not Admin")
        }
        var url: String? = null

        // 从别的请求页面跳转过来的情况，savedRequest不为空
        val savedRequest = requestCache.getRequest(request, response)
        if (savedRequest != null) {
            url = savedRequest.redirectUrl
        }
        logger.info("$savedRequest")
        logger.info("$url")

        // 直接点击登录页面，根据登录用户的权限跳转到不同的页面
        */
/*if (url == null) {
            for (auth in authentication.authorities) {
                url = authDispatcherMap?.get(auth.authority)
            }
            redirectStrategy.sendRedirect(request, response, url)
        }*//*

        redirectStrategy.sendRedirect(request, response, "/")
//        super.onAuthenticationSuccess(request, response, authentication)
    }

    override fun afterPropertiesSet() {
        logger.info("afterPropertiesSet")
    }

    override fun setRequestCache(requestCache: RequestCache?) {
        logger.info("$requestCache")
        this.requestCache = requestCache!!
    }

    companion object {
        private val logger = LoggerFactory.getLogger(AdminLoginSuccessHandler::class.java)
    }
}*/
