package cn.goour.web.base.entity

import org.hibernate.validator.constraints.Range
import java.io.Serializable
import javax.validation.constraints.Min
import javax.validation.constraints.Size

data class PageInfo(
        @field:[Min(value = 1, message = "页数不能小于1")]
        var page: Int = 1,
        @field:[Range(min = 5, max = 20, message = "每页条数只能在5-20之间")]
        var size: Int = 10,
        var search: String = ""
) : Serializable {

}