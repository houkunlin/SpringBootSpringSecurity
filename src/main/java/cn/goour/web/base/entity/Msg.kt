package cn.goour.web.base.entity

import org.springframework.data.domain.Page
import java.util.*
import kotlin.collections.ArrayList

data class Msg(
        var code: Int = 0,
        var title: String? = null,
        var msg: String = "执行成功",
        var msgs: Array<String> = arrayOf(),
        var url: ArrayList<Href> = arrayListOf(),
        var bean: Any? = null,

        var list: Collection<Any>? = null,
        var count: Long = 0,
        var pageSize: Int = 0,
        var pageCurr: Int = 0,
//        @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
        var time: Date = Date(),
        var timestamp: Long = System.currentTimeMillis()
) {
    constructor(code: Int) : this() {
        this.code = code
    }

    constructor(msg: String) : this() {
        this.msg = msg
    }

    constructor(code: Int, msg: String) : this(code) {
        this.msg = msg
    }

    constructor(code: Int, msg: String, msgs: Array<String>) : this(code, msg) {
        this.msgs = msgs
    }

    constructor(code: Int, msg: String, bean: Any?) : this(code, msg) {
        this.bean = bean
    }

    constructor(code: Int, msg: String, url: ArrayList<Href>) : this(code, msg) {
        this.url = url
    }

    constructor(list: Page<out Any>?) : this() {
        this.initPage(list)
    }

    constructor(code: Int, msg: String, list: Page<out Any>?) : this(code, msg) {
        this.initPage(list)
    }

    fun initPage(page: Page<out Any>?) {
        if (page != null) {
            this.msg = "获取列表成功"
            this.count = page.totalElements
            this.pageSize = page.totalPages
            this.pageCurr = page.number + 1
            this.list = initList(page.content)
        }
    }

    private fun initList(page: List<Any>): List<Any> {
        val list = ArrayList<Any>()
        if (page.isNotEmpty()) {
            val first = page[0]

            if (first is JsonItem) {
                list.addAll(getList(page as List<JsonItem>))
            } else {
                list.addAll(page)
            }
        }
        return list
    }

    private fun getList(page: List<JsonItem>): List<Any> {
        val list = ArrayList<Any>()
        page.forEach {
            list.add(it.toJsonItem())
        }
        return list
    }

    companion object {

        fun error() = Msg(1, "未知错误")
        fun error(code: Int) = Msg(code, "未知错误")
        fun error(msg: String) = Msg(1, msg)
        fun error(msg: String, title: String) = Msg(1, msg = msg, title = title)
        fun error(e: Throwable) = Msg(1, "错误：${e.message}")
        fun error(code: Int, e: Throwable) = Msg(code, "错误：${e.message}")
        fun error(e: Throwable, title: String) = Msg(1, msg = "错误：${e.message}", title = title)
        fun error(code: Int, e: Throwable, title: String) = Msg(code, msg = "错误：${e.message}", title = title)
    }
}

interface JsonItem {
    fun toJsonItem(): Map<String, Any?>
}

data class Href(
        var url: String = "",
        var blank: Boolean = false,
        var text: String = ""
) {

}