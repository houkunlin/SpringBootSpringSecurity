package cn.goour.web


import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.web.servlet.ServletComponentScan
import org.springframework.boot.web.servlet.ServletContextInitializer
import org.springframework.scheduling.annotation.EnableAsync
import javax.annotation.PostConstruct
import javax.servlet.ServletContext


@EnableAsync
@SpringBootApplication
@ServletComponentScan
open class App : ServletContextInitializer {

    override fun onStartup(servletContext: ServletContext?) {
        getLog().info("onStartup ${this.javaClass.name}...")
        println("onStartup ${this.javaClass.name}....")
        Initialized.onStartup(servletContext)
    }


    @PostConstruct
    fun post() {
        getLog().info("PostConstruct ${this.javaClass.simpleName}....")
        println("PostConstruct ${this.javaClass.simpleName}....")
    }

    companion object {

        private var logger: Logger? = LoggerFactory.getLogger(App::class.java)

        @JvmStatic
        fun getLog(): Logger {
            if (logger == null) {
                logger = LoggerFactory.getLogger(App::class.java)
            }
            return logger!!
        }

        @JvmStatic
        fun main(args: Array<String>) {
            val app = SpringApplication(App::class.java)
            app.addListeners(A(), B(), C())
            app.run(*args)
        }
    }
}