package cn.goour.web

import format
import org.springframework.boot.context.event.ApplicationPreparedEvent
import org.springframework.boot.context.event.ApplicationReadyEvent
import org.springframework.boot.context.event.ApplicationStartingEvent
import org.springframework.context.ApplicationListener
import java.util.*

var startTime: Long = 0
var endTime: Long = 0

internal class A : ApplicationListener<ApplicationStartingEvent> {

    override fun onApplicationEvent(applicationStartingEvent: ApplicationStartingEvent) {
        startTime = System.currentTimeMillis()
        println("ApplicationStartingEvent")
    }
}

internal class B : ApplicationListener<ApplicationPreparedEvent> {

    override fun onApplicationEvent(applicationPreparedEvent: ApplicationPreparedEvent) {
        println("ApplicationPreparedEvent")
    }
}

internal class C : ApplicationListener<ApplicationReadyEvent> {

    override fun onApplicationEvent(applicationReadyEvent: ApplicationReadyEvent) {
        endTime = System.currentTimeMillis()
        println("ApplicationReadyEvent")
        println(ILoveHoukunlin)
        println("${Date().format()}  -> Spring Boot Spring MVC Server Is Start (longTime = ${(endTime - startTime) / 1000.0}s)")
    }

    companion object {
        @JvmStatic
        fun main(args: Array<String>) {

        }

        val ILoveHoukunlin = """
 _   _             _  __            _     _
| | | |           | |/ /           | |   (_)       ▉▉▉／◢▉◣◢▉◣◥◣／◢◤／／／／／／／／
| |_| | ___  _   _| ' / _   _ _ __ | |    _ _ __   ／▉／／▉／◥◤／▉／◥▉◤／◢▉◣／▉／▉／
|  _  |/ _ \| | | |  < | | | | '_ \| |   | | '_ \  ／▉／／◥◣／／◢◤／／▉／／▉／▉／▉／▉／
| | | | (_) | |_| | . \| |_| | | | | |___| | | | | ／▉／／／◥◣◢◤／／／▉／／▉／▉／▉／▉ ／
|_| |_|\___/ \__,_|_|\_\\__,_|_| |_|____/|_|_| |_| ▉▉▉／／／◥◤／／／／▉／／◥▉◤／◥▉◤／
        """.trimIndent()
    }
}