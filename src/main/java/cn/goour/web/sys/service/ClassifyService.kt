package cn.goour.web.sys.service

import cn.goour.web.sys.entity.Classify
import cn.goour.web.sys.repository.ClassifyRepository
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import javax.annotation.PostConstruct
import javax.transaction.Transactional

@Service
@Transactional
open class ClassifyService {

    @Autowired
    lateinit var classifyRepository: ClassifyRepository

    /**
     * 找到子类
     */
    open fun getSubRole(classify: Classify): List<Classify> {
        val list = ArrayList<Classify>()
        val re = classifyRepository.getByUid(classify.id) ?: return list
        list.addAll(re)
        return list
    }

    /**
     * 获得所有子类
     */
    open fun getSubAllRole(classify: Classify): List<Classify> {
        val list = ArrayList<Classify>()
        val re = classifyRepository.getByUid(classify.id) ?: return list
        re.forEach {
            list.add(it)
            list.addAll(getSubAllRole(it))
        }
        return list
    }

    /**
     * 通过深度获得该深度的子类
     */
    open fun getDepthRole(depth: Int): List<Classify> {
        val list = ArrayList<Classify>()
        val re = classifyRepository.getByDepth(depth) ?: return list
        list.addAll(re)
        return list
    }

    /**
     * 通过深度获得该深度下面的子类
     */
    open fun getDepthAllRole(depth: Int): List<Classify> {
        val list = ArrayList<Classify>()
        val re = classifyRepository.getByDepth(depth) ?: return list
        re.forEach {
            list.add(it)
            list.addAll(getSubAllRole(it))
        }
        return list
    }

    init {
        logger.info("Initialized ${this.javaClass.simpleName}...")
    }

    @PostConstruct
    open fun post() {
        logger.info("PostConstruct ${this.javaClass.simpleName}...")
    }

    companion object {
        private val logger = LoggerFactory.getLogger(RoleService::class.java)
    }
}