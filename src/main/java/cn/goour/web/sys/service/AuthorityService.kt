package cn.goour.web.sys.service

import cn.goour.web.base.`interface`.ServiceBase
import cn.goour.web.sys.entity.RoleGroup
import cn.goour.web.sys.repository.RoleGroupRepository
import cn.goour.web.sys.repository.RoleRepository
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import javax.annotation.PostConstruct
import javax.transaction.Transactional

@Service
@Transactional
open class RoleService {

    @Autowired
    lateinit var roleRepository: RoleRepository


    init {
        logger.info("Initialized ${this.javaClass.simpleName}...")
    }

    @PostConstruct
    open fun post() {
        logger.info("PostConstruct ${this.javaClass.simpleName}...")
    }

    companion object {
        private val logger = LoggerFactory.getLogger(RoleService::class.java)
    }
}

@Service
@Transactional
open class RoleGroupService : ServiceBase<RoleGroup> {

    @Autowired
    private lateinit var roleGroupRepository: RoleGroupRepository

    open fun findAll(): List<RoleGroup> {
        val list = roleGroupRepository.findAll() ?: listOf()

        list.forEach {
            it.roles
        }
        return list
    }

    init {
        logger.info("Initialized ${this.javaClass.simpleName}...")
    }


    override fun add(vo: RoleGroup): RoleGroup {
        return roleGroupRepository.save(vo)
    }

    override fun update(vo: RoleGroup) {
        roleGroupRepository.saveAndFlush(vo)
    }

    override fun delete(ids: Array<Int>) {
        ids.forEach {
            roleGroupRepository.delete(it)
        }
    }


    @PostConstruct
    open fun post() {
        logger.info("PostConstruct ${this.javaClass.simpleName}...")
    }

    companion object {
        private val logger = LoggerFactory.getLogger(RoleGroupService::class.java)
    }
}