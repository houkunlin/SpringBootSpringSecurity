import cn.goour.web.sys.entity.Admin
import cn.goour.web.sys.entity.User
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpSession

fun HttpServletRequest.getUser(): User = this.session.getUser()

@Throws(RuntimeException::class)
fun HttpSession.getUser(): User {
    val obj = this.getAttribute("user")
    if (obj is User) {
        return obj
    }
    throw RuntimeException("session attr key \"user\" is not User")
}

fun HttpServletRequest.setUser(obj: User) {
    this.session.setUser(obj)
}

fun HttpSession.setUser(obj: User) {
    this.setAttribute("user", obj)
}

fun HttpServletRequest.getAdmin(): Admin = this.session.getAdmin()

@Throws(RuntimeException::class)
fun HttpSession.getAdmin(): Admin {
    val obj = this.getAttribute("admin")
    if (obj is Admin) {
        return obj
    }
    throw RuntimeException("session attr key \"admin\" is not Admin")
}

fun HttpServletRequest.setAdmin(obj: Admin) {
    this.session.setAdmin(obj)
}

fun HttpSession.setAdmin(obj: Admin) {
    this.setAttribute("admin", obj)
}