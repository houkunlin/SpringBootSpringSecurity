package cn.goour.web.sys.controller.settings

import cn.goour.web.base.entity.Msg
import cn.goour.web.base.entity.PageInfo
import cn.goour.web.base.exception.BackJsonBeanValidFailException
import cn.goour.web.sys.entity.type.HttpHeader
import cn.goour.web.sys.service.LogService
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.validation.Errors
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import javax.annotation.PostConstruct

@RestController
@RequestMapping("/admin/settings/")
open class AdminLogsController {
    @Autowired private
    lateinit var logService: LogService

    @GetMapping("/adminLogs", headers = arrayOf(HttpHeader.Ajax))
    open fun list(@Validated pageInfo: PageInfo, errors: Errors): Msg {
        if (errors.hasErrors()) {
            throw BackJsonBeanValidFailException(errors)
        }
        val list = logService.getAdminLogs(pageInfo)
        return Msg(list)
    }

    @GetMapping("/adminLogs/admin_{id}", headers = arrayOf(HttpHeader.Ajax))
    open fun getByAdmin(@PathVariable("id") id: Int, @Validated pageInfo: PageInfo, errors: Errors): Msg {
        if (errors.hasErrors()) {
            throw BackJsonBeanValidFailException(errors)
        }
        val list = logService.getAdminLogs(pageInfo, id)
        return Msg(list)
    }

    init {
        logger.info("Initialized ${this.javaClass.simpleName}...")
    }

    @PostConstruct
    open fun post() {
        logger.info("PostConstruct ${this.javaClass.simpleName}...")
    }

    companion object {
        private val logger = LoggerFactory.getLogger(AdminLogsController::class.java)
    }
}