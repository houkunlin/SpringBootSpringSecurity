package cn.goour.web.sys.controller.settings

import cn.goour.web.base.entity.Msg
import cn.goour.web.base.exception.BackJsonBeanValidFailException
import cn.goour.web.sys.entity.Role
import cn.goour.web.sys.entity.RoleGroup
import cn.goour.web.sys.entity.type.HttpHeader
import cn.goour.web.sys.service.RoleGroupService
import decrypt
import getAdmin
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.*
import javax.annotation.PostConstruct
import javax.servlet.http.HttpServletRequest


@Controller
@RequestMapping("/admin/settings/roleGroup")
open class RoleGroupController {

    @Autowired private
    lateinit var roleGroupService: RoleGroupService

    /*@InitBinder
    fun initListBinder(binder: WebDataBinder) {
        // 设置需要包裹的元素个数，默认为256
        binder.autoGrowCollectionLimit = 256
    }*/

    @GetMapping("/index")
    open fun index(model: Model): String {
        logger.info("index string")
        model.addAttribute("roles", Role.roles)
        model.addAttribute("roleDes", Role.roleDes)
        return "web/admin/settings/role"
    }

    @ResponseBody
    @GetMapping("/index", headers = arrayOf(HttpHeader.Ajax))
    open fun list(): Msg {
        logger.info("list index")
        return Msg(list = roleGroupService.findAll())
    }

    /*
    可用的信息,但是我不想用
    @ResponseBody
    @PostMapping("/index", headers = arrayOf(HttpHeader.Ajax))
    open fun modify(roleGroup: RoleGroup, @RequestParam(value = "role[]") role: List<Role>): Msg {
        logger.info("role = {}", role)
        logger.info("roles = {}", roleGroup.roles)
        if (roleGroup.id > 0) {
            roleGroup.roles = role
            roleGroupService.update(roleGroup)
        } else {
            roleGroup.roles = role
            roleGroupService.add(roleGroup)
        }
        return Msg()
    }*/

    @ResponseBody
    @PostMapping("/index", headers = arrayOf(HttpHeader.Ajax))
    open fun modify(roleGroup: RoleGroup): Msg {
        logger.info("roles = {}", roleGroup.roles)
        if (roleGroup.id > 0) {
            roleGroupService.update(roleGroup)
        } else {
            roleGroupService.add(roleGroup)
        }
        return Msg()
    }

    @ResponseBody
    @DeleteMapping("/index", headers = arrayOf(HttpHeader.Ajax))
    open fun delete(id: Int, password: String, request: HttpServletRequest): Msg {
        if (request.getAdmin().password.decrypt() != password) {
            throw BackJsonBeanValidFailException("当前用户密码验证错误")
        }
        roleGroupService.delete(arrayOf(id))
        return Msg()
    }

    init {
        logger.info("Initialized ${this.javaClass.simpleName}...")
    }

    @PostConstruct
    open fun post() {
        logger.info("PostConstruct ${this.javaClass.simpleName}...")
    }

    companion object {
        private val logger = LoggerFactory.getLogger(RoleGroupController::class.java)
    }
}