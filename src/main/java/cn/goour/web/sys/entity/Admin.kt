package cn.goour.web.sys.entity

import cn.goour.web.base.`interface`.ValidAdd
import cn.goour.web.base.`interface`.ValidModifying
import com.fasterxml.jackson.annotation.JsonIgnore
import org.hibernate.validator.constraints.Length
import org.slf4j.LoggerFactory
import java.io.Serializable
import javax.persistence.*
import javax.validation.constraints.Email
import javax.validation.constraints.Min
import javax.validation.constraints.NotBlank
import javax.validation.constraints.Pattern

private val logger = LoggerFactory.getLogger("Admin.kt")
/*
@Entity
data class Admin(
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        var id: Int = 0,
        var name: String = "",
        var email: String = "",
        var username1: String = "",
        var password1: String = "",
        @ManyToMany(cascade = arrayOf(CascadeType.DETACH), fetch = FetchType.EAGER)
        var roles: List<Role> = ArrayList<Role>()
) : Serializable, UserDetails {
    override fun getAuthorities(): MutableCollection<out GrantedAuthority> {
        val auths = roles.mapTo(ArrayList<GrantedAuthority>()) { SimpleGrantedAuthority(it.name) }
        logger.info("$roles")
        logger.info("$auths")
        return auths
    }

    private fun getAuthorities(roles: List<Role>): MutableCollection<out GrantedAuthority> {
        val auths: ArrayList<GrantedAuthority> = ArrayList<GrantedAuthority>()
        roles.forEach {
            auths.add(SimpleGrantedAuthority(it.name))
//            auths.addAll(getAuthorities(it.subRole))
        }
        return auths
    }

    override fun isEnabled(): Boolean {
        return true
    }

    override fun getUsername() = username1

    override fun isCredentialsNonExpired(): Boolean {
        return true
    }

    override fun getPassword() = password1.decrypt()

    override fun isAccountNonExpired(): Boolean {
        return true
    }

    override fun isAccountNonLocked(): Boolean {
        return true
    }

}*/

@Entity
data class Admin(
        @field:[Id GeneratedValue(strategy = GenerationType.IDENTITY) Min(value = 1, message = "参数ID错误", groups = arrayOf(ValidModifying::class))]
        var id: Int = 0,
        @field:[Column(length = 64) NotBlank(message = "姓名不能够为空") Pattern(regexp = "^[\u4e00-\u9fa5A-Za-z0-9_ ]{2,64}\$", message = "姓名只能中文、英文和中英组合，长度在2-64之间")]
        var name: String = "",
        @field:[Column(length = 64) Length(max = 64, message = "电子邮件长度不能大于64个字") NotBlank(message = "电子邮件不能为空") Email(groups = arrayOf(ValidAdd::class, ValidModifying::class), message = "电子邮件格式不正确")]
        var email: String = "",
        var phone: String = "",
        @field:[Column(length = 16, updatable = false, insertable = true) NotBlank(message = "用户名不能为空") Pattern(regexp = "^[a-zA-Z][a-zA-Z0-9_]{2,15}\$", message = "用户名必须以字母开头，可由字母、数字、下划线组成，长度在3-16之间")]
        var username: String = "",
        @field:[Column(length = 256) JsonIgnore]
        var password: String = "",
        @field:[JsonIgnore JoinColumn(name = "admin_id") OneToMany(cascade = arrayOf(CascadeType.ALL), fetch = FetchType.LAZY)]
        var logs: List<Log> = arrayListOf(),
        @field:[ManyToOne(cascade = arrayOf(CascadeType.DETACH), fetch = FetchType.EAGER)]
        var roleGroup: RoleGroup? = RoleGroup(),
        var login: Boolean = true,
        var vip: Int = Admin.defaultVip
) : Serializable {

    fun hasRole(roleName: String): Boolean {
        if (vip == 0) {// 超级管理员不需要继续往下执行
            return true
        }
        return roleGroup?.hasRole(roleName) ?: false
    }

    fun hasAnyRole(vararg roleNames: String): Boolean {
        if (vip == 0) {// 超级管理员不需要继续往下执行
            return true
        }
        return roleGroup?.hasAnyRole(roleNames) ?: false
    }

    override fun toString(): String {
        return "Admin(id=$id, name='$name', email='$email', username='$username', password='$password', login=$login, roleGroup=$roleGroup)"
    }

    companion object {
        var defaultVip: Int = 1000
    }
}
