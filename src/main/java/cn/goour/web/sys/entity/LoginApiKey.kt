package cn.goour.web.sys.entity

import cn.goour.web.sys.entity.type.LoginType
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.validation.constraints.NotBlank

@Entity
data class LoginApiKey(
        @field:[Id GeneratedValue(strategy = GenerationType.IDENTITY)]
        var id:Int=0,
        @field:[NotBlank(message = "AppID不能为空，长度在255以内")]
        var appId:String="",
        @field:[NotBlank(message = "AppKey不能为空，长度在255以内")]
        var appSecret:String="",
        var redirectUri:String="",
        var scope:String="",
        var loginType: LoginType = LoginType.None
){

}