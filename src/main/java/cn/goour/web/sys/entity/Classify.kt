package cn.goour.web.sys.entity

import java.io.Serializable
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id

@Entity
data class Classify(
        @field:[Id GeneratedValue(strategy = GenerationType.IDENTITY)]
        var id: Int = 0,
        var title: String = "",// 权限说明
        // 上级id
        var uid: Int = 0,
        // 深度
        var depth: Int = 0
) : Serializable {

}