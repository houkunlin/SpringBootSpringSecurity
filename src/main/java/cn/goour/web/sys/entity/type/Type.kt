package cn.goour.web.sys.entity.type

/*object LoginType {
    val email = "email"
    val username = "username"
    val phone = "phone"
    val qq = "qq"
    val weixin = "weixin"
    val weixinWeb = "weixinWeb"
    val weiBo = "weiBo"
    val yiBan = "yiBan"
    fun getText(type: String): String = map[type] ?: ""

    private val map = mapOf(
            Pair(this.email, "电子邮件"),
            Pair(this.username, "用户名"),
            Pair(this.phone, "手机号"),
            Pair(this.qq, "QQ"),
            Pair(this.weixin, "微信"),
            Pair(this.weixinWeb, "微信WEB"),
            Pair(this.weiBo, "微博"),
            Pair(this.yiBan, "易班")
    )
}*/

enum class LoginType private constructor(val text: String) {
    None(""), Email("电子邮件"), Username("用户名"), Phone("手机号"), QQ("QQ"), WeiXin("微信"), WeiXinWeb("微信WEB"), WeiBo("微博"), YiBan("易班");
}

enum class ActionType private constructor(val text: String) {
    None("没有记录操作内容"),
    Login("登录"),
    Logout("退出登录"),
    Register("注册"),
    AddLoginType("添加登录方式"),
    SchoolIpAdd("添加内网IP"),
    SchoolIpUpdate("修改内网IP"),
    SchoolIpDel("删除内网IP"),
    SchoolIpSearch("查询内网IP"),
}


object HttpHeader {
    const val Ajax = "X-Requested-With"
}
