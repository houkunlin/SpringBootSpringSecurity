package cn.goour.web.sys.entity

import java.io.Serializable
import javax.persistence.*
import javax.validation.constraints.NotBlank

@Entity
data class RoleGroup(
        @field:[Id GeneratedValue(strategy = GenerationType.IDENTITY)]
        var id: Int = 0,
        @field:[Column(length = 20) NotBlank(message = "名称不能为空，长度20字符内")]
        var name: String = "",
        @field:[ManyToMany(cascade = arrayOf(CascadeType.DETACH), fetch = FetchType.EAGER)]
        var roles: List<Role> = ArrayList<Role>()//这个地方,如果在添加或者更新的时候需要填充这里,则必须这样写,其他初始化方法无效
) : Serializable {
    fun hasRole(roleName: String): Boolean {
        roles.forEach {
            if (it.name == Role.getName(roleName)) return true
        }
        return false
    }

    fun hasAnyRole(roleNames: Array<out String>): Boolean {
        val names = this.getRoleNames(roleNames)
        roles.forEach {
            if (names.contains(it.name)) return true
        }
        return false
    }

    private fun getRoleNames(roleNames: Array<out String>): List<String> {
        val obj = arrayListOf<String>()
        roleNames.forEach {
            obj.add(Role.getName(it))
        }
        return obj
    }
}

@Entity
data class Role(
        @field:[Id GeneratedValue(strategy = GenerationType.IDENTITY)]
        var id: Int = 0,
        var name: String = "",// 权限代码
        var title: String = ""// 权限说明
) : Serializable {
    constructor(name: String) : this() {
        this.name = getName(name)
    }

    constructor(name: String, title: String) : this() {
        this.name = getName(name)
        this.title = title
    }

    companion object {
        private val rolePrefix = "ROLE_"

        fun getName(name: String): String {
            if (name.startsWith(rolePrefix)) {
                return name
            }
            return "$rolePrefix$name"
        }

        val roles = mapOf(
                Pair("管理员",
                        arrayListOf(
                                Role("admin_list", "管理员列表"),
                                Role("admin_add", "添加管理员"),
                                Role("admin_update", "修改管理员"),
                                Role("admin_del", "删除管理员"),
                                Role("admin_group", "修改权限")
                        )
                ),
                Pair("权限组",
                        arrayListOf(
                                Role("r_list", "权限组列表"),
                                Role("r_add", "添加权限组"),
                                Role("r_update", "修改权限组"),
                                Role("r_del", "删除权限组"),
                                Role("r_root", "超级权限")
                        )
                ),
                Pair("内网IP",
                        arrayListOf(
                                Role("ip_list", "IP列表"),
                                Role("ip_add", "添加IP"),
                                Role("ip_update", "修改IP"),
                                Role("ip_del", "删除IP"),
                                Role("ip_import", "导入IP"),
                                Role("ip_export", "导出IP"),
                                Role("ip_clear", "清空IP")
                        )
                )
        )
        val roleDes = mapOf(
                Pair("管理员", "修改权限：可修改其他管理员权限。建议给超级管理员分配此权限，无此权限者无法修改其他管理员权限"),
                Pair("权限组", "超级权限：可以编辑所有权限信息。无超级权限则只能编辑已经拥有的权限信息，拥有权限之外的权限无法编辑。建议只给超级管理员分配此权限")
        )
    }
}
