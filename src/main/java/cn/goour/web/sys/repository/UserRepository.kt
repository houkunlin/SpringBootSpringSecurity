package cn.goour.web.sys.repository

import cn.goour.web.sys.entity.User
import cn.goour.web.sys.entity.UserLoginAuthorize
import cn.goour.web.sys.entity.type.LoginType
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.JpaSpecificationExecutor

interface UserRepository : JpaRepository<User, Int>, JpaSpecificationExecutor<User> {
}

interface UserLoginAuthorizeRepository : JpaRepository<UserLoginAuthorize, Int>, JpaSpecificationExecutor<User> {
    fun getByIdentifierAndLoginType(identifier: String, loginType: LoginType): UserLoginAuthorize

}
