package cn.goour.web.sys.repository

import cn.goour.web.sys.entity.Admin
import cn.goour.web.sys.entity.Log
import cn.goour.web.sys.entity.User
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.JpaSpecificationExecutor


interface LogRepository : JpaRepository<Log, Int>, JpaSpecificationExecutor<Log> {
    fun findByIdAndAdmin(id: Int, admin: Admin): Log?
    fun findByIdAndUser(id: Int, user: User): Log?
}
