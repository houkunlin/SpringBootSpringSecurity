package cn.goour.web.sys.repository

import cn.goour.web.sys.entity.Classify
import cn.goour.web.sys.entity.Role
import cn.goour.web.sys.entity.RoleGroup
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.JpaSpecificationExecutor

interface RoleRepository : JpaRepository<Role, Int>, JpaSpecificationExecutor<Role> {
}

interface RoleGroupRepository : JpaRepository<RoleGroup, Int>, JpaSpecificationExecutor<RoleGroup> {
}
