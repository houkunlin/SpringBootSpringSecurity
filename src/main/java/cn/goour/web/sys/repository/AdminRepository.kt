package cn.goour.web.sys.repository

import cn.goour.web.sys.entity.Admin
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.JpaSpecificationExecutor

interface AdminRepository : JpaRepository<Admin, Int>, JpaSpecificationExecutor<Admin> {
    fun getByUsername(username: String): Admin?

}