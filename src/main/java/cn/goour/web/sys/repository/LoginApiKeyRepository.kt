package cn.goour.web.sys.repository

import cn.goour.web.sys.entity.LoginApiKey
import cn.goour.web.sys.entity.type.LoginType
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.JpaSpecificationExecutor

interface LoginApiKeyRepository : JpaRepository<LoginApiKey, Int>, JpaSpecificationExecutor<LoginApiKey> {
    fun getByLoginType(loginType: LoginType): LoginApiKey
}