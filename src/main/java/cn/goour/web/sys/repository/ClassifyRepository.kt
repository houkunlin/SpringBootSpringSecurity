package cn.goour.web.sys.repository

import cn.goour.web.sys.entity.Classify
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.JpaSpecificationExecutor


interface ClassifyRepository : JpaRepository<Classify, Int>, JpaSpecificationExecutor<Classify> {
    /**
     * 通过父类id找到子类内容
     */
    fun getByUid(uid: Int): List<Classify>?

    /**
     * 通过深度depth找到子类内容
     */
    fun getByDepth(depth: Int): List<Classify>?
}