package cn.goour.web.dser.repository

import cn.goour.web.dser.entity.SchoolIp
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import org.springframework.data.jpa.repository.Modifying
import org.springframework.data.jpa.repository.Query

interface SchoolIpRepository : JpaRepository<SchoolIp, Int>, JpaSpecificationExecutor<SchoolIp> {
    fun getByBuildingAndRoom(building: String, room: String): SchoolIp?
    @Modifying
    @Query("DELETE FROM SchoolIp")
    fun clearTable()
}