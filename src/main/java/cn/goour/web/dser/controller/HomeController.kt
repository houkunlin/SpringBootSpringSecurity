package cn.goour.web.dser.controller

import cn.goour.web.base.`interface`.ValidAdd
import cn.goour.web.base.entity.Msg
import cn.goour.web.base.exception.BackJsonBeanValidFailException
import cn.goour.web.sys.entity.Admin
import cn.goour.web.sys.service.AdminService
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.validation.BindingResult
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.ResponseBody
import javax.annotation.PostConstruct
import javax.servlet.http.HttpServletRequest
import javax.validation.Valid
import javax.validation.groups.Default


@Controller
@RequestMapping("/")
open class HomeController {
    @Autowired private
    lateinit var adminService: AdminService

    @RequestMapping("/")
    open fun index(model: Model, request: HttpServletRequest): String {
        val msg = Msg(1, title = "测试标题", msg = "额外信息，只对管理员显示")
        model.addAttribute("msg", msg)
        return "index"
    }

    @ResponseBody
    @RequestMapping("/add")
    @Throws(Exception::class)
    open fun index3(model: Model, request: HttpServletRequest): Msg {
        if (request.method == "POST") {
            return Msg()
        }
        throw Exception("从控制器传来的错误信息")
    }


    @ResponseBody
    @RequestMapping("/add1", method = arrayOf(RequestMethod.POST), headers = arrayOf("X-Requested-With"))
    open fun add1(@Valid admin: Admin, bindingResult: BindingResult): Msg {
        if (bindingResult.hasErrors()) {
            show(bindingResult)
            throw BackJsonBeanValidFailException(bindingResult)
        }
        logger.info("添加：$admin")
        adminService.add(admin)
        return Msg()
    }

    private fun show(bindingResult: BindingResult) {
        println(bindingResult.fieldError.defaultMessage)
        println(bindingResult.fieldErrors)
        println(bindingResult.allErrors)
        println(bindingResult.errorCount)
        println(bindingResult.globalErrorCount)
        /*bindingResult.allErrors.forEach {
            println()
            println()
            println()
            println(it)
            println(it.javaClass.name)
            println(it.defaultMessage)
            println(it.objectName)
            println(it.code)
            println(it.arguments)
            it.arguments.forEach {
                println("-->" + it.javaClass.name)
                println("-->" + it)
            }
        }*/
    }

    @ResponseBody
    @RequestMapping("/add11", method = arrayOf(RequestMethod.POST), headers = arrayOf("X-Requested-With"))
    open fun add11(admin: Admin, bindingResult: BindingResult): Msg {
        if (bindingResult.hasErrors()) {
            show(bindingResult)
            throw BackJsonBeanValidFailException(bindingResult)
        }
        logger.info("添加：$admin")
        adminService.add(admin)
        return Msg()
    }

    @ResponseBody
    @RequestMapping("/add111", method = arrayOf(RequestMethod.POST), headers = arrayOf("X-Requested-With"))
    open fun add111(@Validated(ValidAdd::class, Default::class) admin: Admin, bindingResult: BindingResult): Msg {
        if (bindingResult.hasErrors()) {
            show(bindingResult)
            throw BackJsonBeanValidFailException(bindingResult)
        }
        logger.info("添加：$admin")
        adminService.add(admin)
        return Msg()
    }

    init {
        logger.info("Initialized ${this.javaClass.simpleName}...")
    }

    @PostConstruct
    open fun post() {
        logger.info("PostConstruct ${this.javaClass.simpleName}...")
        logger.info("$adminService")
    }

    companion object {
        private val logger = LoggerFactory.getLogger(HomeController::class.java)
    }
}