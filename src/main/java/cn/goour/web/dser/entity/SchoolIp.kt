package cn.goour.web.dser.entity

import cn.goour.web.base.`interface`.ValidModifying
import java.io.Serializable
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.validation.constraints.Min
import javax.validation.constraints.NotBlank


@Entity
data class SchoolIp(
        @field:[Id GeneratedValue(strategy = GenerationType.IDENTITY) Min(value = 1, message = "IP参数ID错误", groups = arrayOf(ValidModifying::class))]
        var id: Int = 0,
        @field:NotBlank(message = "建筑物不能为空！")
        var building: String = "",
        @field:NotBlank(message = "房间号不能为空！")
        var room: String = "",
        @field:NotBlank(message = "起始IP不能为空！")
        var ip1: String = "",
        @field:NotBlank(message = "终止IP不能为空！")
        var ip2: String = "",
        @field:NotBlank(message = "网关不能为空！")
        var gw: String = "",//网关
        @field:NotBlank(message = "子网掩码不能为空！")
        var mask: String = "",//子网掩码
        var dns1: String = "",
        var dns2: String = ""
) : Serializable {

    fun toMap(): HashMap<String, String> {
        return hashMapOf(
                Pair("id", id.toString()),
                Pair("building", building),
                Pair("room", room),
                Pair("ip1", ip1),
                Pair("ip2", ip2),
                Pair("gw", gw),
                Pair("mask", mask),
                Pair("dns1", dns1),
                Pair("dns2", dns2)
        )
    }
}