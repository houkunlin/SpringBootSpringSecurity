package cn.goour.web.dser.aspect

import org.aspectj.lang.annotation.*
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Component

@Aspect
@Component
open class HttpAspect {

    /**
     * 切面，面向切面编程，使用AOP处理请求
     */
    @Pointcut("execution(public * cn.goour.web.dser.controller.HomeController.*(..))")
    open fun log() {

    }

    /**
     * 在请求处理前执行
     */
    @Before("log()")
    open fun before() {
        logger.info("before")
    }

    /**
     * 在请求处理后执行
     */
    @After("log()")
    open fun after() {
        logger.info("after")
    }

    /**
     * 获取从控制器方法return的结果
     */
    @AfterReturning(pointcut = "log()", returning = "obj")
    open fun afterReturning(obj: Any?) {
        logger.info("afterReturning = "+obj)
    }

    companion object {
        private val logger = LoggerFactory.getLogger(HttpAspect::class.java)
    }
}