/*
 * Copyright (c) $year.
 * 网址：http://goour.cn
 * 作者：侯坤林
 * 邮箱：lscode@qq.com
 * 侯坤林 版权所有
 *
 */

package cn.goour.web

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.boot.builder.SpringApplicationBuilder
import org.springframework.boot.web.support.SpringBootServletInitializer
import javax.annotation.PostConstruct
import javax.servlet.ServletContext


//@EnableAsync
//@SpringBootApplication
//@ServletComponentScan
open class AppServer : SpringBootServletInitializer() {

    override fun configure(builder: SpringApplicationBuilder?): SpringApplicationBuilder {
        builder!!.listeners(A(), B(), C())
        return builder.sources(AppServer::class.java);
    }

    override fun onStartup(servletContext: ServletContext?) {
        getLog().info("onStartup ${this.javaClass.name}...")
        println("onStartup ${this.javaClass.name}....")
        super.onStartup(servletContext)
        Initialized.onStartup(servletContext)
    }


    @PostConstruct
    fun post() {
        getLog().info("PostConstruct ${this.javaClass.simpleName}....")
        println("PostConstruct ${this.javaClass.simpleName}....")
    }

    companion object {

        private var logger: Logger? = LoggerFactory.getLogger(AppServer::class.java)

        @JvmStatic
        fun getLog(): Logger {
            if (logger == null) {
                logger = LoggerFactory.getLogger(AppServer::class.java)
            }
            return logger!!
        }
    }
}