# WUEFL说明文件
完整版请查看[官方文档](https://docs.scientiamobile.com/documentation/onsite/onsite-java-api "Title") 

### IS_APP 类型：枚举
>   告诉你请求的HTTP客户端是否是应用程序。控制功能被称为
    **`controlcap_is_app`**（**`virtual_capability`**组），
    并且可以具有的值default，force_true并force_false

###is_smartphone 类型：枚举
>    这是一个虚拟功能，将告诉您设备是否是智能手机，
    用于ScientiaMobile对智能手机的任意（并可能更改）定义。
    虚拟能力返回true或false。补丁文件可以使用**`is_smartphone`**控制功能来覆盖虚拟功能返回的值。
    控制能力，**`is_smartphone`**可以采取值default，force_true和force_false。

### is_robot 类型：枚举
>    这是一个虚拟功能，可以告诉您HTTP Client是否是Bot（机器人，爬网程序或其他可编程的代理网络）。
    控制能力是**`is_robot`**（**`virtual_capability`**组），并且可以具有的值default，force_true和force_false。

### IS_MOBILE 类型：枚举
>	这只是一个ALIAS is_wireless_device。没有与此虚拟功能相关联的控制功能。

### is_full_desktop 类型：枚举
>	这只是一个ALIAS ux_full_desktop。没有与此虚拟功能相关联的控制功能。

### is_windows_phone 类型：枚举
>    检查设备是否运行任何版本的Windows Phone OS。该虚拟功能依赖于device_os（product_info组）功能。

### is_ios 类型：枚举
>    检查设备是否运行任何版本的iOS。该虚拟功能依赖于device_os（product_info组）功能。

### is_android 类型：枚举
>    检查设备是否运行任何版本的Android操作系统。该虚拟功能依赖于device_os（product_info组）功能。

### is_touchscreen 类型：枚举
>    此虚拟功能会告诉您设备是否具有触摸屏。没有控制能力。大多数为pointing_method== touchscreen（product_info组）功能的别名。

### is_largescreen 类型：枚举
>    如果设备的水平和垂直屏幕分辨率大于480像素，则为真。依赖于resolution_width和resolution_height（display集团）功能。

### is_wml_preferred 类型：枚举
>    如果设备更好地使用WML，则为true。能力依赖preferred_markup（markup组）。

### is_xhtmlmp_preferred 类型：枚举
>    如果设备更好地与XHTML MP（移动配置文件）配合，则为true。能力依赖preferred_markup（标记组）。

### is_html_preferred 类型：枚举
>    如果设备更好地使用HTML服务，则为true。能力依赖preferred_markup（标记组）。

### advertised_device_os 类型：串
>    该虚拟功能将基于用户代理字符串分析（以及可能的其他HTTP头和WURFL功能的分析）来推断设备操作系统的名称。

### advertised_device_os_version	串
>	该虚拟功能将基于用户代理字符串分析（以及可能的其他HTTP头和WURFL功能的分析）来推断设备操作系统的版本。

### advertised_browser 类型：串
>    该虚拟功能将基于用户代理字符串分析（以及可能的其他HTTP头和WURFL功能的分析）来推断浏览器的名称。

### advertised_browser_version 类型：串
>	该虚拟功能将基于用户代理字符串分析（以及可能的其他HTTP头和WURFL功能的分析）来推断浏览器的版本。

### form_factor 类型：串
>	这个虚拟能力将返回标识客户端的外形以下值之一：Desktop，Tablet，Smartphone，Feature Phone，Smart-TV，Robot，Other non-Mobile，Other Mobile

### complete_device_name 类型：串
>    将设备的品牌名称，型号名称和营销名称（如果可用）连接到单个字符串中。

### is_phone 类型：枚举
>    这是一个虚拟功能，可以告诉您设备是否是手机。
    虚拟能力返回true或false。补丁文件可以使用**`is_phone`**控制功能来覆盖虚拟功能返回的值。
    控制能力，is_phone可以采取值default，force_true和force_false。

### is_app_webview 类型：枚举
>    如果HTTP请求来自基于应用程序的Webview，则此虚拟功能将返回true。

### device_name 类型：串
>    将设备的品牌名称和营销名称连接成一个字符串。如果营销名称不可用，则使用型号名称。

### advertised_app_name 类型：串
>	该虚拟功能将返回生成User-Agent或HTTP请求的应用程序的名称。